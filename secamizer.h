#ifndef SECAMIZER_H
#define SECAMIZER_H

/**************************************************************************
*   Copyright (C) 2013-2022 by Moritz Strohm                              *
*   ncc1988@posteo.de                                                     *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

//started work at 2013-10-02 23:44

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>


unsigned long Secamize_YV12(unsigned char* yv12Data,
                            unsigned short imageWidth,
                            unsigned short imageHeight,
                            unsigned char flareFactor,
                            unsigned char colorNoiseFactor,
                            unsigned char flareTreshold
                           )
{
  /**
   * Puts a Secam-like Effect on the U and V components
   * 
   * yv12Data is a pointer to a YV12 picture (without headers). Its content will be overwritten by Secamize.
   * imageWidth and imageHeight have to match the image dimensions to prevent segfaults when processing the image data.
   * flareFactor is an adjustable value to tell Secamize() how strong the secam flare shall appear (0 = none).
   * colorNoiseFactor lets you adjust how much noise will be added to the "color carrier". It may lead to green dots
   * where you can see green stripes instead of red or blue ones.
   * If the difference between two luma pixels in a row is higher than flareTreshold, secam flare will appear
   * This function returns the amount of processed luma pixels from yv12Data.
   **/
  
  if(yv12Data == NULL || imageWidth == 0 || imageHeight == 0 || ((imageHeight % 2) != 0))
  {
    //we can't secamize the picture if no data are given, the image has no width or height or if the height is not a multiple of two
    return 0;
  }
  
  //initialize the random noise generator for the color noise factor
  //(we're not doing cryptography, so we don't need anything special as seed)
  srand(time(NULL));
  
  unsigned char uOnFlare = 0; //if the U value is "firing", the V value must not flare. used as boolean (0 = false, 1 = true)
  
  unsigned long rowNumber = 0; //the number of the Y pixel row in the picture
  unsigned long pixelCounter = 0; //a counter for the Y pixels
  
  unsigned long offsetU = imageWidth*imageHeight;
  unsigned long offsetV = offsetU + (offsetU)/4;
  
  for(rowNumber = 0; rowNumber < imageHeight-1; rowNumber++)
  {
    short flareValue = 0; //this is responsible for adding the secam flare to U and V.
                         //it is reset on each line because secam flare doesn't spread to the next line
    
    if((rowNumber % 2) == 0)
    {
      //we have reached a new pair of lines: reset uOnFlare
      uOnFlare = 0;
    }
    
    unsigned char lastValueY = yv12Data[rowNumber*imageWidth]; //this holds the last Y value in the row. It is set to the first Y pixel value in the row
    
    unsigned long column = 0; //the Y pixel column inside a row
    for(column = 0; column < imageWidth; column++)
    {
      unsigned long uvRelativePosition = ((rowNumber/2)* (imageWidth/2)+(column/2));
      unsigned char* currentY = yv12Data+(rowNumber*imageWidth+column);
      unsigned char* currentU = yv12Data+(offsetU)+uvRelativePosition;
      unsigned char* currentV = yv12Data+(offsetV)+uvRelativePosition;
      
      if(flareValue == 0)
      {
        //we don't start a new secam flare if one is already "burning"
        if((lastValueY - *currentY) < -flareTreshold)
        {
          //(when the luma value turns from dark to light values (low to high)
          flareValue = (*currentY - lastValueY) * flareFactor/32;
        }
        //no else block here
      }
      
      lastValueY = *currentY; //for the next pixel
      
      //color noise calculation: either -1, 0 or +1
      short noise = (((rand() % 3)-1) * colorNoiseFactor);
      
      //overflow prevention for U...
      if(*currentU + noise > 255)
      {
        *currentU = 255;
      }
      else if (*currentU + noise < 0)
      {
        *currentU = 0;
      }
      else
      {
        *currentU += noise;
      }
      
      //... and V
      if(*currentV + noise > 255)
      {
        *currentV = 255;
      }
      else if (*currentV + noise < 0)
      {
        *currentV = 0;
      }
      else
      {
        *currentV += noise;
      }
      
      
      //now add the flare value to either U or V, but not on both:
      //first check the conditions for U:
      if((rowNumber % 2) == 0)
      {
        //apply flare to the U values:
        if((*currentU + flareValue) > 255)
        {
          *currentU = 255;
        }
        else
        {
          *currentU += flareValue;
        }
      }
      else
      {
        //apply flare to the V values:
        if((*currentV + flareValue) > 255)
        {
          *currentV = 255;
        }
        else
        {
          *currentV += flareValue;
        }
      }
      
      //decrease or increase the flare value to make it fade:
      if(flareValue > 0)
      {
        flareValue--;
      }
      else if(flareValue < 0)
      {
        flareValue++;
      }
      
      pixelCounter++;
    }
  }
  
  return pixelCounter;
}


unsigned long Secamize(unsigned char* RGBData, unsigned short ImageWidth, unsigned short ImageHeight, unsigned char FlareFactor, unsigned char colorNoiseFactor)
{
  /**
   * Puts an Secam-like Effect on the picture after its horizontal color resolution (R + B) was resized to half the resolution.
   * 
   * RGBInput is a pointer to valid RGB data. Its content will be overwritten by Secamize.
   * ImageWidth and ImageHeight have to match the image dimensions to prevent segfaults when processing the image data.
   * FlareFactor is an adjustable value to tell Secamize() how strong the secam flare shall appear (0 = none).
   * colorNoiseFactor lets you adjust how much noise will be added to the "color carrier". It may lead to green dots
   * where you can see green stripes instead of red or blue ones.
   * This function returns the amount of processed bytes from RGBData.
   **/
  
  unsigned long ByteCounter = 0;
  
  if(RGBData == NULL || ImageWidth == 0 || ImageHeight == 0 || ((ImageHeight % 2) != 0))
  {
    //if RGBData is a NULL pointer, the Image width or height is zero or if ImageHeight isn't a multiple of two
    //we can't secamize a picture.
    return 0;
  }
  
  /*unsigned char* GBuf = (unsigned char*) malloc(ImageWidth);
  //green buffer (GBuf) needed for secam flare calculation
  
  if(GBuf == NULL)
  {
    return 0;
  }
  
  memset(GBuf, 0, ImageWidth);
  */
  
  
  //initialize the random noise generator
  //(we're not doing cryptography, so we don't need anything special as seed)
  srand(time(NULL));
  
  short FlareValue = 0;
  unsigned char FlareTreshold = 64; //treshold value when secam flare will appear if the value is above the treshold
  unsigned char PreviousLineOnFlare = 0;
  
  unsigned long Line = 0;
  for(Line = 0; Line < ImageHeight-1; Line++)
  {
    FlareValue = 0; //reset the flare value on each new line
    unsigned char LastValueG = RGBData[ByteCounter+1]; //set it to the first G value of the line
    
    /*if(PreviousLineOnFlare > 1)
    {
      PreviousLineOnFlare = 0;
    }
    else
    {
      PreviousLineOnFlare++;
    }*/
    
    PreviousLineOnFlare = 0;
      
    unsigned long Row = 0;
    for(Row = 0; Row < ImageWidth; Row++)
    {
      //ByteCounter points to the Red value here
      
      unsigned char* CurrentLineR = RGBData+ByteCounter;
      unsigned char* NextLineR = RGBData+ByteCounter+(ImageWidth*3);
      unsigned char* CurrentLineB = RGBData+ByteCounter+2;
      unsigned char* NextLineB = RGBData+ByteCounter+2+(ImageWidth*3);
      unsigned char CurrentG = RGBData[ByteCounter+1];
      
      if(FlareValue == 0)
      {
        //you can only start a new flare if the old burned down
        if((LastValueG - CurrentG) > FlareTreshold)
        {
          //when the green value turns from light to dark values (high to low)
          //DISABLED! FlareValue = (LastValueG - CurrentG) * FlareFactor/32;
        }
        else if((LastValueG - CurrentG) < -FlareTreshold)
        {
          //when the green value turns from dark to light values (low to high)
          FlareValue = (CurrentG - LastValueG) * FlareFactor/32;
        }
      }
      
      LastValueG = CurrentG;
      //LastValueG = 0;
      
      //normal calculation:
      //unsigned short SecamR = (unsigned short)((*CurrentLineR + *NextLineR) / 2);
      //unsigned short SecamB = (unsigned short)((*CurrentLineB + *NextLineB) / 2);

      //calculation with color noise:
      short noise = (((rand() % 3)-1) * colorNoiseFactor);
      unsigned short SecamR = (unsigned short)((*CurrentLineR + *NextLineR) / 2);
      unsigned short SecamB = (unsigned short)((*CurrentLineB + *NextLineB) / 2);
      
      //the following prevents overflow for red...
      if(SecamR + noise > 255)
      {
        SecamR = 255;
      }
      else if (SecamR + noise < 0)
      {
        SecamR = 0;
      }
      else
      {
        SecamR += noise;
      }
      
      //... and for blue:
      if(SecamB + noise > 255)
      {
        SecamB = 255;
      }
      else if (SecamB + noise < 0)
      {
        SecamB = 0;
      }
      else
      {
        SecamB += noise;
      }
      
      //add flare value to one of the both lines, if the other line hasn't got it yet:
      if((PreviousLineOnFlare < 2))
      {
        PreviousLineOnFlare = 1;
        if((Line % 2) == 0)
        {
          //red line:
          SecamR += FlareValue;
          
          if(SecamR > 255)
          {
            SecamR = 255;
          }
        }
        else if((Line % 2) == 1)
        {
          //blue line:
          SecamB += FlareValue;
          if(SecamB > 255)
          {
            SecamB = 255;
          }
        }
      }
      
      *NextLineR = SecamR;
      *CurrentLineR = SecamR;
      *NextLineB = SecamB;
      *CurrentLineB = SecamB;
      
      if(FlareValue > 0)
      {
        FlareValue --;
      }
      else if(FlareValue < 0)
      {
        FlareValue ++;
      }
      
      ByteCounter += 3; //hop over R,G and B value
    }
  }
  
  return ByteCounter;
  
}


#endif
