
// resources:
// http://www.avidemux.org/admWiki/doku.php?id=tutorial:writing_your_own_filter
// http://avisynth.nl/index.php/FAQ_YV12

static FILTER_PARAM secamizer_param = {2, "flare", "colornoise"};

typedef struct secamizerParam
{
  uint8_t flare;
  uint8_t colornoise;
}secamizerParam;


class Secamizer: public AVDMGenericVideoStream
{
  public:
    char* printConf(void);
    uint8_t getCoupledConf(CONFcouple** couples);
    uint8_t configure(AVDMGenericVideoStream* in);
    uint8_t getFrameNumberNoAlloc(uint32_t inframe, uint32_t* len, ADMImage* data, uint32_t* flags);
};
