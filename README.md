## secamizer readme

Secamizer is a console application that adds the SECAM flare effect to digital
images. The SECAM flare is a disturbance from the analog TV standard SECAM
that produced red and blue stripes at sharp edges of the image.

Secamizer tries to reproduce this effect on digital images.

At the moment secamizer can only handle raw RGB-24 images.
To convert ordinary images, the shell script easysecam.sh
is included that demonstrates the effect that secamizer produces.
That shell script needs imagemagick to be installed.

### requirements

* C compiler (clang, gcc or compatible)
* make
* C standard libraries
* ImageMagick (for easysecam.sh)

### build

If you just type make, the C compiler of your distribution may be used.
If you prefer a certain compiler like gcc, you have to call make and
specify the executable of your compiler:

    make CC=gcc


### usage

secamizer accepts the following arguments:
* --help: print help
* --in: set input file (default: stdin)
* --out: set output file (default: stdout)
* --size: set image size in width x height (e.g. 640x480)
* --ffactor: set SECAM flare factor. Lower values mean less secam flare. Numbers in the range [0..255] are accepted. (default: 16)
* --cnfactor: set color noise factor. This adds some noise to the color signals. Numbers in the range [0.255] are accepted (default:1)

Example: secamizer --size=640x480 --ffactor=16 --cnfactor=2
