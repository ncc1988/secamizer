## secamizer readme

Secamizer is a console application that adds a SECAM fire to digital images.
The SECAM fire is a disturbance from the analog TV standard SECAM
that produced red and blue stripes at sharp edges of the image.

Secamizer tries to reproduce this effect on digital images.

At the moment secamizer can only handle raw RGB-24 images.
To convert ordinary images the shell script easysecam.sh
is included.

### requirements

* C compiler (clang, gcc or compatible)
* make
* C standard libraries
* ImageMagick (for easysecam.sh)

### build

If you use clang you can just type make
to compile secamizer. If you prefer gcc
you have to open the Makefile and replace the line
CC=clang
with
CC=gcc

Now you can build secamizer with gcc.

### usage

secamizer accepts the following arguments:
* --help: print help
* --in: set input file (default: stdin)
* --out: set output file (default: stdout)
* --size: set image size in width x height (e.g. 640x480)
* --ffactor: set SECAM fire factor. Lower values mean less secam fire. Numbers in the range [0..255] are accepted. (default: 16)
* --cnfactor: set color noise factor. This adds some noise to the color signals. Numbers in the range [0.255] are accepted (default:1)

Example: secamizer --size=640x480 --ffactor=16 --cnfactor=2

