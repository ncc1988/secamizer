#!/bin/bash

if [ $# -lt 1 ]
then
  echo "Usage: video-secamize.sh INFILE [RESIZE]"
  echo "INFILE has to be a video format understood by FFmpeg"
  echo "RESIZE is the optional new video size (WxH)"
  exit 1
fi

mkdir ./src_jpg
mkdir ./secam_jpg

size=0
if [ $# -eq 1 ]
then
  size=$(ffmpeg -i "$1" 2>&1 | grep "Stream #0:0" | cut -d, -f4 | tr -d ' ')
elif [ $# -eq 2 ]
then
  size=$2
fi

fps=$(ffmpeg -i "$1" 2>&1 | grep "Stream #0:0" | cut -d, -f6 | tr -d ' ' | sed "s/fps$//g")

if [ $# -eq 1 ]
then
  ffmpeg -i "$1" -qscale 1 -f image2 ./src_jpg/%07d.jpg
elif [ $# -eq 2 ]
then
  ffmpeg -i "$1" -qscale 1 -s $size -f image2 ./src_jpg/%07d.jpg
fi

if [ $? -ne 0 ]
then
  echo "FFmpeg error!"
  exit 1
fi

frames=$(ls -1 ./src_jpg/*.jpg | wc -l)

i=0

cd ./src_jpg

echo -e "\n\nStarting SECAM conversion!"

for f in *.jpg
do
  fname=$(echo "$f" | sed "s/.jpg$//g")
  ffmpeg -v 0 -i "$fname.jpg" -pix_fmt yuv420p -f rawvideo - | secamizer --ffactor=8 --cnfactor=1 --size=$size | ffmpeg -v 0 -s $size -pix_fmt yuv420p -f rawvideo -i - -f image2 -y "../secam_jpg/$fname.jpg"
  if [ $? -eq 0 ]
    then
      rm "$fname.jpg"
    else
      echo "ffmpeg error!"
    exit 1
  fi
  i=$(expr $i + 1)
  echo -ne "Frames converted: $i/$frames\r"
done

echo -e "\n SECAM conversion finished!"

cd ../secam_jpg

ffmpeg -r $fps -i %07d.jpg -vn -i "../$1" -codec:v mjpeg -qscale 2 -codec:a copy -map 0:0 -map 1:1 -y "../$1.secam.mkv"
if [ $? -ne 0 ]
then
  echo "FFmpeg error!"
  exit 1
fi

cd ..
rm -r ./src_jpg ./secam_jpg

echo "video-secamize.sh: finished conversion of file "$1"!"
exit 0