/**************************************************************************
*   Copyright (C) 2013-2022 by Moritz Strohm                              *
*   ncc1988@posteo.de                                                    *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 3 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

//started work at 2013-10-02 23:44

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "secamizer.h"

#define USE_YV12 1

enum
{
  NO_OPTION,
  FLAREFACTOR_OPTION,
  FLAREFACTOR_VALUE,
  NOISEFACTOR_OPTION,
  NOUSEFACTOR_VALUE
};

void PrintHelp()
{
  fputs("Usage: secamizer OPTIONS INFILE OUTFILE\n", stderr);
  fputs("INFILE: Filename for input RGB data, - means read from stdin\n", stderr);
  fputs("OUTFILE: Filename for output RGB data, - means write to stdout\n", stderr);
}

int main(int argc, char *argv[])
{
  char* inFileName = NULL;
  char* outFileName = NULL;
  unsigned char flareFactor = 0;
  unsigned char colorNoiseFactor = 0;
  
  unsigned long imageWidth = 0;
  unsigned long imageHeight = 0;
  
  //fprintf(stderr, "%d\n", argc); //debug
  
  if(argc > 1)
  {
    //handle help option:
    if(strcmp(argv[1], "--help") == 0)
    {
      PrintHelp();
      return 0;
    }
    else
    {
      //handle all other options:
      unsigned char i = 2;
      unsigned char parseState = NO_OPTION;
      
      for(i = 1; i < argc; i++)
      {
        unsigned char validArg = 0;
        
        char* argument = strtok(argv[i],"=");
        if(strcmp(argument, "--in") == 0)
        {
          inFileName = (char*) malloc(strlen(&argv[i][5]));
          strcpy(inFileName, &argv[i][5]);
          validArg = 1;
        }
        else if(strcmp(argument, "--out") == 0)
        {
          outFileName = (char*) malloc(strlen(&argv[i][6]));
          strcpy(outFileName, &argv[i][6]);
          validArg = 1;
        }
        else if(strcmp(argument, "--size") == 0)
        {
          sscanf(argv[i]+7, "%dx%d", &imageWidth, &imageHeight);
          validArg = 1;
        }
        else if(strcmp(argument, "--ffactor") == 0)
        {
          flareFactor = (unsigned char)atoi(argv[i]+10);
          validArg = 1;
        }
        else if(strcmp(argument, "--cnfactor") == 0)
        {
          colorNoiseFactor = (unsigned char)atoi(argv[i]+11);
          validArg = 1;
        }
        
        
        if(validArg != 1)
        {
          fputs("ERROR: Invalid argument specified!\n", stderr);
          PrintHelp();
          return 1;
        }
      }
    }
  }
  else
  {
    PrintHelp();
    return 1;
  }
  
  FILE *in = NULL;
  FILE *out = NULL;
  
  //set the input file name, if given. Otherwise: stdin
  if(inFileName == NULL)
  {
    in = stdin;
  }
  else
  {
    in = fopen(inFileName, "r");
  }
  
  //set the output file name, if given. Otherwise: stdout
  if(outFileName == NULL)
  {
    out = stdout;
  }
  else
  {
    out = fopen(outFileName, "w");
  }
  
  //handle errors when opening the input and output file (or stream)
  if(in == NULL)
  {
    if(inFileName != NULL)
    {
      fprintf(stderr, "ERROR: Cannot read from input file \"%s\"!\n", inFileName);
    }
    else
    {
      fputs("ERROR: Cannot read from input stream!\n", stderr);
    }
    return 1;
  }
  if(out == NULL)
  {
    if(inFileName != NULL)
    {
      fprintf(stderr, "ERROR: Cannot write to output file \"%s\"!\n", inFileName);
    }
    else
    {
      fputs("ERROR: Cannot write to output stream!\n", stderr);
    }
    fclose(in);
    return 1;
  }
  
  //set default image size if the size argument wasn't given:
  if(imageWidth == 0 || imageHeight == 0)
  {
    fputs("NOTE: Image size not set or invalid. Assuming default image size 640x480\n", stderr);
    imageWidth = 640;
    imageHeight = 480;
  }
  
  //set default value for flare factor, if it was not set via argument:
  if(flareFactor == 0)
  {
    flareFactor = 16;
  }
  
  //set default value for the color noise factor, if it was not set via argument:
  if(colorNoiseFactor == 0)
  {
    colorNoiseFactor = 1;
  }
  
  unsigned long allocSize = (imageWidth*imageHeight*3);
  
  //fprintf(stderr, "DEBUG: malloc size: %d\n", allocSize);
  
  unsigned char* Data = (unsigned char*) malloc((int)allocSize);
  if(Data == NULL)
  {
    fputs("ERROR: Couldn't allocate memory!\n", stderr);
    fclose(in);
    fclose(out);
    return 1;
  }
#ifdef USE_YV12
  //YV12
  fread(Data, imageWidth*imageHeight*1.5,1,in);
  unsigned long convertedPixels = Secamize_YV12(Data, imageWidth, imageHeight, flareFactor, colorNoiseFactor, 64);
  fwrite(Data, imageWidth*imageHeight*1.5,1,out);
  //fprintf(stderr, "DEBUG: %u of %u Bytes were converted!\n", convertedPixels, (unsigned long)(imageWidth*imageHeight*1.5));
#else
  //RGB24
  fread(Data, imageWidth*imageHeight*3,1,in);
  unsigned long convertedPixels = Secamize_YV12(Data, imageWidth, imageHeight, flareFactor, colorNoiseFactor);
  //fprintf(stderr, "DEBUG: %ld of %ld Bytes were converted!\n", convertedPixels, (imageWidth*imageHeight*3));
  
  fwrite(Data, imageWidth*imageHeight*3,1,out);
#endif
  
  
  fclose(in);
  fclose(out);
  
  return 0;
}
