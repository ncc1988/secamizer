
if [ $# -lt 2 ]
then
  echo "Usage: easysecam.sh INFILE OUTFILE\n"
  echo "Example: easysecam.sh input.jpg output.png\n\n"
  exit 1
fi

#$1 = infile, $2 = outfile

size=$(identify -format "%wx%h" "$1" | cut -d' ' -f3)

cmd="./secamizer"
$cmd 2>/dev/null #we just want to see if the program is there
if [ $? -eq 127 ]
then
  cmd="secamizer" #program is not in current directory... look in one of the bin directories instead
fi

convert "$1" -depth 8 -sampling-factor 4:2:0 -interlace plane yuv:- | $cmd --size=$size --ffactor=8 --cnfactor=1 | convert -depth 8 -sampling-factor 4:2:0 -interlace plane -size $size yuv:- -quality 0.9 "$2"
